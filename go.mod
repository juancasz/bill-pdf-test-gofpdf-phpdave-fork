module bill-pdf-test-gofpdfphpdave

go 1.16

require (
	github.com/boombuler/barcode v1.0.1 // indirect
	github.com/phpdave11/gofpdf v1.4.2
	github.com/phpdave11/gofpdi v1.0.13 // indirect
	github.com/ruudk/golang-pdf417 v0.0.0-20201230142125-a7e3863a1245 // indirect
)
