package main

import (
	"fmt"

	"bill-pdf-test-gofpdfphpdave/codeGenerator"

	"github.com/phpdave11/gofpdf"
	"github.com/phpdave11/gofpdf/contrib/barcode"
	"github.com/phpdave11/gofpdf/contrib/gofpdi"
)

func main() {
	GTIN := "7701234000011"
	referenceNumber := "500000067"
	paymentValue := "0006007358"
	maxPaymentDate := "20210605"

	GenerateBarcodePDF(GTIN, referenceNumber, paymentValue, maxPaymentDate, 132, 17)
}

//GenerateBarcodePDF generate and print a barcode into a PDF
//GTIN: 13 characters length string. All the characters must be numbers. Required
//referenceNumber: string from 2 to 24 characters. All the characters must be numbers. Required
//paymentValue: string from 2 to 12 characters. Decimal notation allowed (For example 123.123). Optional. If not needed pass parameter as an empty string
//maxPaymentDate: 8 characters length string. Format AAAAMMDD. Optional. If not needed pass parameter as an empty string
//width: mm width of the barcode
//height: mm height of the barcode
func GenerateBarcodePDF(GTIN string, referenceNumber string, paymentValue string, maxPaymentDate string, width float64, height float64) {

	code, err := codeGenerator.GenerateCode(GTIN, referenceNumber, paymentValue, maxPaymentDate)

	fmt.Println("Creating barcode:", code)

	if err != nil {
		fmt.Println("barcode can't be generated:", err)
		return
	}

	pdf := createPdf()

	fmt.Println("Filling template...")

	pdf.SetFont("Helvetica", "UI", 12)
	pdf.SetTextColor(150, 0, 0)

	//for testing purposes
	pdf.MoveTo(50, 80)
	pdf.Cell(50, 50, "Texto de Prueba")
	//////////

	//create and position the barcode
	xPositionBarCode := (210 - width) / 2
	key := barcode.RegisterCode128(pdf, code)
	barcode.Barcode(pdf, key, xPositionBarCode, 203, width, height, false)
	///////////

	//write the string related to the barcode just below the barcode
	pdf.SetFont("Helvetica", "", 10)
	pdf.SetTextColor(0, 0, 0)
	pdf.MoveTo(10, 203+height)
	pdf.CellFormat(0, 5, code, "", 1, "C", false, 0, "")
	////////

	err = pdf.OutputFileAndClose("bill.pdf")
	if err != nil {
		panic(err)
	}
	fmt.Println("PDF file saved")
}

func createPdf() (pdf *gofpdf.Fpdf) {
	pdf = gofpdf.New("P", "mm", "A4", "")

	fmt.Println("Importing PDF template...")

	// Import example-pdf.pdf with gofpdi free pdf document importer
	tpl1 := gofpdi.ImportPage(pdf, "FacturaConvenio.pdf", 1, "/MediaBox")

	pdf.AddPage()

	// Draw imported template onto page
	gofpdi.UseImportedTemplate(pdf, tpl1, 0, 0, 210, 297)

	return
}
